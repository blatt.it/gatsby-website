/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// Load IBM Plex Mono typeface
require("typeface-ibm-plex-mono")

// Load IBM Plex Serif typeface
require("typeface-ibm-plex-serif")

// Load IBM Plex Sans typeface
require("typeface-ibm-plex-sans")

require("prismjs/themes/prism-okaidia.css")

// import './src/css/prism-duotone-dark.css'
