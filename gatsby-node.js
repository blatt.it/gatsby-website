/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require("path")

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return new Promise((resolve, reject) => {
    graphql(`
      {
        allCockpitBlogpost {
          edges {
            node {
              cockpitId
              slug {
                value
              }
            }
          }
        }
      }
    `).then(result => {
      result.data.allCockpitBlogpost.edges.map(({ node }) => {
        createPage({
          // can query data specific to each page.
          path: `/blog/${node.slug.value}/`,
          component: path.resolve(`./src/templates/BlogPost.js`),
          context: {
            slug: node.slug.value,
          },
        })
      })
      resolve()
    })
  })
}
