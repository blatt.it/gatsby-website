import { StaticQuery, graphql } from "gatsby"

import Helmet from "react-helmet"
import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"
import tw from "tailwind.macro"
import Header from "./header"
import GlobalStyle from "../styles/global"

const Content = styled.div`
  ${tw`px-32 py-8 font-sans`};
`
const Footer = styled.div`
  ${tw`flex mb-4`};
`
const Credits = styled.p`
  ${tw`flex justify-between max-w-4xl mx-auto p-4 md:p-8 text-sm px-32 py-8 font-sans`};
`
const StyledLink = styled.a`
  ${tw`no-underline text-900`};
`

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: "description", content: "Sample" },
            { name: "keywords", content: "sample, something" },
          ]}
        >
          <html lang="de" />
        </Helmet>
        <GlobalStyle />
        <Header siteTitle={data.site.siteMetadata.title} />
        <Content>{children}</Content>
        <Footer>
          <Credits>
            &copy; {new Date().getFullYear()} &nbsp;
            <StyledLink href="https://blatt.it">Daniel Blatt</StyledLink>
          </Credits>
        </Footer>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
