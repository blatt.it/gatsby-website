import React from "react"

import { Link, graphql } from "gatsby"

import Layout from "../components/layout"

const BlogPost = ({ data }) => (
  <Layout>
    {data.allCockpitBlogpost.edges.map(({ node }) => (
      <div>
        <h1>{node.title.value}</h1>
        <div
          dangerouslySetInnerHTML={{
            __html: node.content.value.childMarkdownRemark.html,
          }}
        />
        <hr />
      </div>
    ))}
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
)

export const query = graphql`
  query BlogPostTemplateQuery($slug: String!) {
    allCockpitBlogpost(
      filter: { slug: { value: { eq: $slug } }, lang: { eq: "de" } }
    ) {
      edges {
        node {
          title {
            value
          }
          content {
            value {
              childMarkdownRemark {
                html
              }
            }
          }
        }
      }
    }
  }
`

export default BlogPost
