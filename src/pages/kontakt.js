import { Link } from "gatsby"
import React from "react"
import styled from "styled-components"
import tw from "tailwind.macro"

import Layout from "../components/layout"

const Button = styled.button`
  ${tw`bg-blue hover:bg-blue-lighter text-white font-bold py-2 px-4 border-b-4 border-blue-darker hover:border-blue rounded`};
`
const Label = styled.label`
  ${tw`block font-bold mb-2 text-xs uppercase`};
`
const Input = styled.input`
  ${tw`appearance-none block bg-gray-200 mb-6 px-3 py-2 rounded-md text-gray-700 w-full`};
`
const Textarea = styled.textarea`
  ${tw`appearance-none bg-gray-200 mb-6 px-3 py-2 rounded-md text-gray-700 w-full`};
`

const ContactPage = () => (
  <Layout>
    <form method="post" action="#" className="mx-auto md:w-1/2">
      <p className="leading-loose mb-8">
        Wenn du mir etwas sagen willst, kannst du mir gerne Schreiben. Wenn du
        technische Probleme auf der Seite feststellst, kannst du gerne direkt in
        GitLab ein{" "}
        <a
          href="https://gitlab.com/blatt.it/gatsby-website/issues"
          className="font-bold no-underline text-gray-700"
        >
          Ticket
        </a>{" "}
        erstellen.
      </p>
      <Label htmlFor="name">
        Name
        <Input type="text" name="name" id="name" placeholder="Dein Name" />
      </Label>
      <Label>
        Email
        <Input
          type="email"
          name="email"
          id="email"
          placeholder="email@adresse.tld"
        />
      </Label>
      <Label>
        Nachricht
        <Textarea
          name="message"
          id="message"
          rows="8"
          placeholder="Deine Nachricht..."
        />
      </Label>
      <Button type="submit">Absenden</Button>
      {/* <input type="reset" value="Clear" /> */}
    </form>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default ContactPage
