import React from "react"

import { Link, graphql } from "gatsby"

import Layout from "../components/layout"

const IndexPage = ({ data }) => (
  <Layout>
    {data.allCockpitBlogpost.edges.map(({ node }) => (
      <div>
        <h1>
          <Link to={`/blog/${node.slug.value}`}>{node.title.value}</Link>
        </h1>
        {/* <div
          dangerouslySetInnerHTML={{
            __html: node.content.value.childMarkdownRemark.html,
          }}
        /> */}
        <p>{node.content.value.childMarkdownRemark.excerpt}</p>
        <hr />
      </div>
    ))}
    <Link to="/page-2/">Go to page 2</Link>
    <Link to="/kontakt/">Go to Kontakt</Link>
  </Layout>
)

export const query = graphql`
  query IndexQuery {
    allCockpitBlogpost(
      filter: { lang: { eq: "de" } }
      sort: { order: DESC, fields: cockpitCreated }
    ) {
      edges {
        node {
          cockpitBy
          cockpitCreated
          cockpitModified
          title {
            value
          }
          tags {
            value
          }
          slug {
            value
          }
          lang
          cockpitId
          content {
            value {
              childMarkdownRemark {
                html
                timeToRead
                wordCount {
                  paragraphs
                  sentences
                  words
                }
                excerpt
              }
            }
          }
          preview {
            value {
              publicURL
              name
            }
          }
        }
      }
    }
  }
`

export default IndexPage
