import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"

import Layout from "../components/layout"

const SecondPage = () => {
  const data = useStaticQuery(query)

  return (
    <Layout>
      <h1>Hi from the second page</h1>
      <p>Welcome to page 2</p>

      <p>This site was last built on: {data.site.buildTime}</p>
      <Link to="/">Go back to the homepage</Link>
    </Layout>
  )
}

export default SecondPage

const query = graphql`
  query Info {
    site {
      buildTime(formatString: "DD.MM.YYYY")
    }
  }
`
