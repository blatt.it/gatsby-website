// eslint-disable-next-line
require("dotenv").config()

module.exports = {
  siteMetadata: {
    title: "blatt.it",
    description: "My personal blog",
    author: "Daniel Blatt",
  },
  plugins: [
    "gatsby-plugin-sharp",
    "gatsby-plugin-styled-components",
    "gatsby-plugin-react-helmet",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "gatsby-tailwind-styled-components-starter",
        short_name: "starter",
        start_url: "/",
        background_color: "#663399",
        theme_color: "#663399",
        display: "minimal-ui",
        icon: "src/images/gatsby-icon.png", // This path is relative to the root of the site.
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "src",
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: "@fika/gatsby-source-cockpit",
      options: {
        token: process.env.COCKPIT_API_TOKEN,
        baseUrl: process.env.COCKPIT_API_URL,
        locales: ["de"],
        collections: [],
        brokenImageReplacement: null,
      },
    },
    {
      resolve: "gatsby-plugin-nprogress",
      options: {
        color: "darkgreen",
        showSpinner: true,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          "gatsby-remark-smartypants",
          "gatsby-remark-responsive-iframe",
          "gatsby-remark-autolink-headers",
          "gatsby-remark-images",
          "gatsby-plugin-catch-links",
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: "language-",
              inlineCodeMarker: null,
              aliases: {},
            },
          },
        ],
      },
    },
    /* Must be placed at the end */
    "gatsby-plugin-offline",
  ],
}
